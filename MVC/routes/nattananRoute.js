const express = require('express');
const router = express.Router();

const customerController = require('../Controllers/nattananController');

const validator = require('../controllers/nattananValidator');
router.post('/prem/update2/:id',validator.addValidator,customerController.edit2);
router.get('/prem/update/:id',validator.addValidator,customerController.edit);
router.post('/prem/add',validator.addValidator,customerController.save);
router.get('/prem',customerController.list);
router.get('/prem/new',customerController.new);
router.get('/prem/prem2/:id',customerController.del);
router.get('/prem/delete/:id',customerController.delete);



module.exports = router;
