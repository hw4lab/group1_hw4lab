const express = require('express');
const router = express.Router();

const customerController = require('../Controllers/wittayaController');
const validator1 = require('../controllers/wittayaValidator');

router.post('/wit/update2/:id',validator1.addvalidator,customerController.edit2);
router.get('/wit/update/:id',validator1.addvalidator,customerController.edit);
router.post('/wit/add',validator1.addvalidator,customerController.save);
router.get('/wit',customerController.list);
router.get('/wit/new',customerController.new);
router.get('/wit/wit2/:id',customerController.del);
router.get('/wit/delete/:id',customerController.delete);



module.exports = router;
