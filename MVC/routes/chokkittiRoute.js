const express = require('express');
const router = express.Router();

const customerController = require('../Controllers/chokkittiController');

const validator = require('../controllers/chokkittiValidator');
router.post('/oat/update2/:id',validator.addValidator,customerController.edit2);
router.get('/oat/update/:id',validator.addValidator,customerController.edit);
router.post('/oat/add',validator.addValidator,customerController.save);
router.get('/oat',customerController.list);
router.get('/oat/new',customerController.new);
router.get('/oat/oat2/:id',customerController.del);
router.get('/oat/delete/:id',customerController.delete);



module.exports = router;
