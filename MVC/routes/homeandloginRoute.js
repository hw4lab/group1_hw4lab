const express = require('express');
const router = express.Router();

const homeController = require('../controllers/homeandloginController');
const validator = require('../controllers/loginvalitator');

router.get('/', homeController.log);
router.post('/login', validator.loginValidator,homeController.inLogin);
router.get('/home', homeController.logcom);
router.get('/logout', homeController.logout);


module.exports = router;
