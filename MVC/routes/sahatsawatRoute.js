const express = require('express');
const router = express.Router();

const customerController = require('../Controllers/sahatsawatController');

const validator = require('../controllers/sahatsawatValidator');
router.post('/wave/update2/:id',validator.addValidator,customerController.edit2);
router.get('/wave/update/:id',validator.addValidator,customerController.edit);
router.post('/wave/add',validator.addValidator,customerController.save);
router.get('/wave',customerController.list);
router.get('/wave/new',customerController.new);
router.get('/wave/wave2/:id',customerController.del);
router.get('/wave/delete/:id',customerController.delete);



module.exports = router;
