const express = require('express');
const body = require('body-parser');
const cookie = require('cookie-parser');
const session = require('express-session');
const mysql = require('mysql');
const connection = require('express-myconnection')
const app = express();

app.use(express.static('public'));
app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(body.urlencoded({ extended: true }));
app.use(cookie());
app.use(session({
    secret: 'Passw0rd',
    resave: true,
    saveUninitialized: true
}));
app.use(connection(mysql, {
    host: 'localhost',
    user: 'root',
    password: '123456',
    port: 3306,
    database: 'f2k1'
}, 'single'));

const routewit=require('./routes/wittayaRoute');
app.use('/',routewit);
const routeoat=require('./routes/chokkittiRoute');
app.use('/',routeoat);
const routewave=require('./routes/sahatsawatRoute');
app.use('/',routewave);
const routeprem=require('./routes/nattananRoute');
app.use('/',routeprem);
const routexx=require('./routes/homeandloginRoute');
app.use('/',routexx);
app.listen('8082');
