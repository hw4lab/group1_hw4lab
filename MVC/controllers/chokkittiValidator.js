const { check } = require('express-validator');
exports.addValidator = [check('oat',   "oatไม่ถูกต้อง").not().isEmpty(),
                      check('kanthayong',         "kanthayongไม่ถูกต้อง").isFloat(),
                      check('D611998003',   "D611998003ไม่ถูกต้อง").isInt()
                      //check('id_sahatsawat',        "id_sahatsawatไม่ถูกต้อง").not().isEmpty(),
                      ];
