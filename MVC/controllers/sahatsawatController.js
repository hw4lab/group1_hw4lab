
const controller = {};

const { validationResult } = require('express-validator');



controller.new = (req,res) => {

  const data=null;
    if(req.session.user){
  req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM chokkitti;',(err,sahatsawatdata) => {
        res.render('./sahatsawat/sahatsawatForm',{
          data2:sahatsawatdata,
          data1:data,
          session:req.session
        });
      });
    });
  }else {
  res.redirect('/login');
  }};

controller.save = (req,res) => {
  const data=req.body;
  if(data.id_chokkitti==""){
    data.id_chokkitti=null;
  }
  const errors =validationResult(req);
  if(req.session.user){
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      const data=null;
      res.redirect('/wave/new');
    }else {
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลสำเร็จ";
      req.getConnection((err,conn) =>{
        conn.query('INSERT INTO sahatsawat set ?',[data],(err,routes) =>{
          if(err){
            res.json(err);
          }
          console.log(routes);
          res.redirect('/wave');
        });
      });
    }
  }else {
  res.redirect('/login');
  }};

  controller.edit2 = (req,res) => {
    const {id} =req.params;
    const data=req.body;
    if(data.id_chokkitti==""){
      data.id_chokkitti=null;
    }
    const errors =validationResult(req);
  if(req.session.user){
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      req.getConnection((err,conn)=>{
          conn.query('SELECT * FROM sahatsawat WHERE id= ?',[id],(err,wittayaData)=>{
              conn.query('SELECT * FROM chokkitti',(err,sahatsawatData)=>{
                res.render('../views/sahatsawat/sahatsawatForm',{
                  session: req.session,
                  data1:wittayaData[0],
                  data2:sahatsawatData});
              });
          });
      });
      }else {
      req.session.success=true;
      req.session.topic="แก้ข้อมูลสำเร็จ";
      req.getConnection((err,conn) =>{
      conn.query('UPDATE sahatsawat SET ? WHERE id = ?',[data,id],(err,routes) =>{
          if(err){
            res.json(err);
          }
          res.redirect('/wave');
      });
      });
      }
  }else {
  res.redirect('/login');
  }};

controller.list = (req,res) => {
    if(req.session.user){
  req.getConnection((err,conn) => {
    conn.query(' select s.id sid, wave, promson, D611998022, oat, kanthayong, D611998003 from sahatsawat s left join chokkitti on s.id_chokkitti = chokkitti.id; ',(err,wittaya) => {
      if(err){
        res.json(err);
      }
    res.render('../views/sahatsawat/sahatsawat',{
      data:wittaya,
      session:req.session

    });
    });
  });
}else {
res.redirect('/login');
}};



controller.del = (req,res) => {
    const { id } = req.params;
      if(req.session.user){
      req.getConnection((err,conn) => {
        conn.query('SELECT * FROM sahatsawat WHERE id = ?',[id],(err,wittaya) => {
          if(err){
              res.json(err);
          }
       res.render('../views/sahatsawat/sahatsawatDelete',{
         data:wittaya[0],
         session:req.session

       });
     });
   });
 }else {
 res.redirect('/login');
 }};

 controller.delete = (req,res) => {
     const { id } = req.params;
       if(req.session.user){
       req.getConnection((err,conn) => {
         conn.query('DELETE FROM sahatsawat WHERE id = ?',[id],(err,wittaya) => {
           if(err){
               res.json(err);
           }
           console.log('../views/sahatsawat/sahatsawat');
           res.redirect('/wave');
        });
     });
   }else {
   res.redirect('/login');
   }};

  controller.edit = (req,res) => {
    const { id } = req.params;
     if(req.session.user){
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM sahatsawat WHERE id= ?',[id],(err,wittayaData)=>{
                    conn.query('SELECT * FROM chokkitti',(err,sahatsawatData)=>{
                      res.render('../views/sahatsawat/sahatsawatForm',{
                        session: req.session,
                        data1:wittayaData[0],
                        data2:sahatsawatData});
                    });
                });
            });
          }else {
          res.redirect('/login');
          }}


module.exports = controller;
