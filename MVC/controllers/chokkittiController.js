
const controller = {};

const { validationResult } = require('express-validator');



controller.new = (req,res) => {

  const data=null;
    if(req.session.user){
  req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM nattanan ;',(err,sahatsawatdata) => {
        res.render('./chokkitti/chokkittiForm',{
          data2:sahatsawatdata,
          data1:data,
          session:req.session
        });
      });
    });
  }else {
  res.redirect('/login');
  }};

controller.save = (req,res) => {
  const data=req.body;
  if(data.id_nattanan==""){
    data.id_nattanan=null;
  }
  const errors =validationResult(req);
  if(req.session.user){
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      const data=null;
      res.redirect('/oat/new');
    }else {
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลสำเร็จ";
      req.getConnection((err,conn) =>{
        conn.query('INSERT INTO chokkitti set ?',[data],(err,routes) =>{
          if(err){
            res.json(err);
          }
          console.log(routes);
          res.redirect('/oat');
        });
      });
    }
  }else {
  res.redirect('/login');
  }};

  controller.edit2 = (req,res) => {
    const {id} =req.params;
    const data=req.body;
    if(data.id_nattanan==""){
      data.id_nattanan=null;
    }
    const errors =validationResult(req);
  if(req.session.user){
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      req.getConnection((err,conn)=>{
          conn.query('SELECT * FROM chokkitti WHERE id= ?',[id],(err,oatA)=>{
              conn.query('SELECT * FROM nattanan',(err,premB)=>{
                res.render('../views/chokkitti/chokkittiForm',{
                  session: req.session,
                  data1:oatA[0],
                  data2:premB});
              });
          });
      });
      }else {
      req.session.success=true;
      req.session.topic="แก้ข้อมูลสำเร็จ";
      req.getConnection((err,conn) =>{
      conn.query('UPDATE chokkitti SET ? WHERE id = ?',[data,id],(err,routes) =>{
          if(err){
            res.json(err);
          }
          res.redirect('/oat');
      });
      });
      }
  }else {
  res.redirect('/login');
  }};

controller.list = (req,res) => {
    if(req.session.user){
  req.getConnection((err,conn) => {
    conn.query('select s.id sid, oat, kanthayong, D611998003, prem, thananchai, D611998005 from chokkitti s left join nattanan on s.id_nattanan = nattanan.id; ',(err,wittaya) => {
      if(err){
        res.json(err);
      }
    res.render('../views/chokkitti/chokkitti',{
      data:wittaya,
      session:req.session

    });
    });
  });
}else {
res.redirect('/login');
}};



controller.del = (req,res) => {
    const { id } = req.params;
      if(req.session.user){
      req.getConnection((err,conn) => {
        conn.query('SELECT * FROM chokkitti WHERE id = ?',[id],(err,wittaya) => {
          if(err){
              res.json(err);
          }
       res.render('../views/chokkitti/chokkittiDelete',{
         data:wittaya[0],
         session:req.session

       });
     });
   });
 }else {
 res.redirect('/login');
 }};

 controller.delete = (req,res) => {
     const { id } = req.params;
       if(req.session.user){
       req.getConnection((err,conn) => {
         conn.query('DELETE FROM chokkitti WHERE id = ?',[id],(err,wittaya) => {
           if(err){
               res.json(err);
           }
           console.log('../views/chokkitti/chokkitti');
           res.redirect('/oat');
        });
     });
   }else {
   res.redirect('/login');
   }};

  controller.edit = (req,res) => {
    const { id } = req.params;
     if(req.session.user){
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM chokkitti WHERE id= ?',[id],(err,wittayaData)=>{
                    conn.query('SELECT * FROM nattanan',(err,sahatsawatData)=>{
                      res.render('../views/chokkitti/chokkittiForm',{
                        session: req.session,
                        data1:wittayaData[0],
                        data2:sahatsawatData});
                    });
                });
            });
          }else {
          res.redirect('/login');
          }}


module.exports = controller;
