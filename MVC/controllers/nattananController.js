
const controller = {};

const { validationResult } = require('express-validator');



controller.new = (req,res) => {

  const data=null;
    if(req.session.user){
  req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM wittaya;',(err,wittayadata) => {
        res.render('./nattanan/nattananform',{
          data2:wittayadata,
          data1:data,
          session:req.session
        });
      });
    });
  }else {
  res.redirect('/login');
  }};

controller.save = (req,res) => {
  const data=req.body;
  if(data.id_wittaya==""){
    data.id_wittaya=null;
  }
  const errors =validationResult(req);
  if(req.session.user){
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      const data=null;
      res.redirect('/prem/new');
    }else {
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลสำเร็จ";
      req.getConnection((err,conn) =>{
        conn.query('INSERT INTO nattanan set ?',[data],(err,routes) =>{
          if(err){
            res.json(err);
          }
          console.log(routes);
          res.redirect('/prem');
        });
      });
    }
  }else {
  res.redirect('/login');
  }};

  controller.edit2 = (req,res) => {
    const {id} =req.params;
    const data=req.body;
    if(data.id_wittaya==""){
      data.id_wittaya=null;
    }
    const errors =validationResult(req);
  if(req.session.user){
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      req.getConnection((err,conn)=>{
          conn.query('SELECT * FROM nattanan WHERE id= ?',[id],(err,nattananData)=>{
              conn.query('SELECT * FROM wittaya',(err,wittayadata)=>{
                res.render('../views/nattanan/nattananForm',{
                  session: req.session,
                  data1:nattananData[0],
                  data2:wittayadata});
              });
          });
      });
      }else {
      req.session.success=true;
      req.session.topic="แก้ข้อมูลสำเร็จ";
      req.getConnection((err,conn) =>{
      conn.query('UPDATE nattanan SET ? WHERE id = ?',[data,id],(err,routes) =>{
          if(err){
            res.json(err);
          }
          res.redirect('/prem');
      });
      });
      }
  }else {
  res.redirect('/login');
  }};

controller.list = (req,res) => {
    if(req.session.user){
  req.getConnection((err,conn) => {
    conn.query('SELECT p.id as pid,p.prem as pprem,p.thananchai as pthananchai,p.D611998005 as pD611998005,p.id_wittaya as pid_wittaya,w.id as wid, w.wit as wwit,w.kasonkatika as wkasonkatika,w.D611998018 as wD611998018,w.id_sahatsawat as wid_sahatsawat FROM nattanan AS p LEFT JOIN wittaya AS w ON p.id_wittaya=w.id ',(err,nattanan) => {
      if(err){
        res.json(err);
      }
    res.render('../views/nattanan/nattanan',{
      data:nattanan,
      session:req.session

    });
    });
  });
}else {
res.redirect('/login');
}};



controller.del = (req,res) => {
    const { id } = req.params;
      if(req.session.user){
      req.getConnection((err,conn) => {
        conn.query('SELECT * FROM nattanan WHERE id = ?',[id],(err,nattanan) => {
          if(err){
              res.json(err);
          }
       res.render('../views/nattanan/nattananDelete',{
         data:nattanan[0],
         session:req.session

       });
     });
   });
 }else {
 res.redirect('/login');
 }};

 controller.delete = (req,res) => {
     const { id } = req.params;
       if(req.session.user){
       req.getConnection((err,conn) => {
         conn.query('DELETE FROM nattanan WHERE id = ?',[id],(err,nattanan) => {
           if(err){
               res.json(err);
           }
           console.log('../views/nattanan/nattanan');
           res.redirect('/prem');
        });
     });
   }else {
   res.redirect('/login');
   }};

  controller.edit = (req,res) => {
    const { id } = req.params;
     if(req.session.user){
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM nattanan WHERE id= ?',[id],(err,nattananData)=>{
                    conn.query('SELECT * FROM wittaya',(err,wittayadata)=>{res.render('../views/nattanan/nattananForm',{
                        session: req.session,
                        data1:nattananData[0],
                        data2:wittayadata});
                    });
                });
            });
          }else {
          res.redirect('/login');
          }}


module.exports = controller;
