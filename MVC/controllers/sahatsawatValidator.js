const { check } = require('express-validator');
exports.addValidator = [check('wave',   "waveไม่ถูกต้อง").not().isEmpty(),
                      check('promson',         "promsonไม่ถูกต้อง").isFloat(),
                      check('D611998022',   "D611998022ไม่ถูกต้อง").isInt()
                      //check('id_sahatsawat',        "id_sahatsawatไม่ถูกต้อง").not().isEmpty()
                      ];
