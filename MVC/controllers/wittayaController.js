
const controller = {};

const { validationResult } = require('express-validator');



controller.new = (req,res) => {

  const data=null;
    if(req.session.user){
  req.getConnection((err,conn) =>{
      conn.query('SELECT * FROM sahatsawat;',(err,sahatsawatdata) => {
        res.render('./wittaya/wittayaForm',{
          data2:sahatsawatdata,
          data1:data,
          session:req.session
        });
      });
    });
  }else {
  res.redirect('/login');
  }};

controller.save = (req,res) => {
  const data=req.body;
  if(data.id_sahatsawat==""){
    data.id_sahatsawat=null;
  }
  const errors =validationResult(req);
  if(req.session.user){
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      const data=null;
      res.redirect('/wit/new');
    }else {
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลสำเร็จ";
      req.getConnection((err,conn) =>{
        conn.query('INSERT INTO wittaya set ?',[data],(err,routes) =>{
          if(err){
            res.json(err);
          }
          console.log(routes);
          res.redirect('/wit');
        });
      });
    }
  }else {
  res.redirect('/login');
  }};

  controller.edit2 = (req,res) => {
    const {id} =req.params;
    const data=req.body;
    if(data.id_sahatsawat==""){
      data.id_sahatsawat=null;
    }
    const errors =validationResult(req);
  if(req.session.user){
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      req.getConnection((err,conn)=>{
          conn.query('SELECT * FROM wittaya WHERE id= ?',[id],(err,wittayaData)=>{
              conn.query('SELECT * FROM sahatsawat',(err,sahatsawatData)=>{
                res.render('../views/wittaya/wittayaForm',{
                  session: req.session,
                  data1:wittayaData[0],
                  data2:sahatsawatData});
              });
          });
      });
      }else {
      req.session.success=true;
      req.session.topic="แก้ข้อมูลสำเร็จ";
      req.getConnection((err,conn) =>{
      conn.query('UPDATE wittaya SET ? WHERE id = ?',[data,id],(err,routes) =>{
          if(err){
            res.json(err);
          }
          res.redirect('/wit');
      });
      });
      }
  }else {
  res.redirect('/login');
  }};

controller.list = (req,res) => {
    if(req.session.user){
  req.getConnection((err,conn) => {
    conn.query(' select s.id sid, wit, kasonkatika, D611998018, wave, promson, D611998022 from wittaya s left join sahatsawat on s.id_sahatsawat = sahatsawat.id; ',(err,wittaya) => {
      if(err){
        res.json(err);
      }
    res.render('../views/wittaya/wittaya',{
      data:wittaya,
      session:req.session

    });
    });
  });
}else {
res.redirect('/login');
}};



controller.del = (req,res) => {
    const { id } = req.params;
      if(req.session.user){
      req.getConnection((err,conn) => {
        conn.query('SELECT * FROM wittaya WHERE id = ?',[id],(err,wittaya) => {
          if(err){
              res.json(err);
          }
       res.render('../views/wittaya/wittayaDelete',{
         data:wittaya[0],
         session:req.session

       });
     });
   });
 }else {
 res.redirect('/login');
 }};

 controller.delete = (req,res) => {
     const { id } = req.params;
       if(req.session.user){
       req.getConnection((err,conn) => {
         conn.query('DELETE FROM wittaya WHERE id = ?',[id],(err,wittaya) => {
           if(err){
               res.json(err);
           }
           console.log('../views/wittaya/wittaya');
           res.redirect('/wit');
        });
     });
   }else {
   res.redirect('/login');
   }};

  controller.edit = (req,res) => {
    const { id } = req.params;
     if(req.session.user){
            req.getConnection((err,conn)=>{
                conn.query('SELECT * FROM wittaya WHERE id= ?',[id],(err,wittayaData)=>{
                    conn.query('SELECT * FROM sahatsawat',(err,sahatsawatData)=>{res.render('../views/wittaya/wittayaForm',{
                        session: req.session,
                        data1:wittayaData[0],
                        data2:sahatsawatData});
                    });
                });
            });
          }else {
          res.redirect('/login');
          }}


module.exports = controller;
